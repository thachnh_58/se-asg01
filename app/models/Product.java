package models;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.avaje.ebean.Page;
import play.data.format.Formats;
import play.mvc.PathBindable;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.libs.F.Option;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class Product extends Model implements PathBindable<Product> {

    @Id
    public Long id;

    @Constraints.Required
    public String ean;

    @Constraints.Required
    public String name;
    public String description;

    //@Formats.DateTime(pattern = "yyyy-MM-dd")
    public Date date;
    public byte[] picture;

    @Override
    public Product bind(String key, String value) {
        return findByEan(value);
    }

    @Override
    public String unbind(String key){
        return this.ean;
    }

    @Override
    public String javascriptUnbind(){
        return this.ean;
    }

    @OneToMany(mappedBy = "product")
    public List<StockItem> stockItems;

    @ManyToMany
    public List<Tag> tags;

    public Product(){}
    public Product(String ean, String name, String description){
        this.ean = ean;
        this.name = name;
        this.description = description;
    }

    public String toString(){
        return String.format("%s - %s", ean, name);
    }

    public static Finder<Long,Product> find = new Finder<Long, Product>(Long.class, Product.class);

    public static List<Product> findAll(){
        return find.all();
    }

    public static Product findByEan(String ean){
        return find.where().eq("ean", ean).findUnique();
    }

    public static List<Product> findByName(String term){
        return find.where().eq("name", term).findList();
    }

    public static Page<Product> find(int page) {
        return find.where()
                .orderBy("id asc") //tang dan id
                .findPagingList(5) //kich thuoc trang
                .setFetchAhead(false) //co can lay tat ca du lieu mot the
                .getPage(page); //lay trang hien tai
    }

    public void delete() {
        for (Tag tag : tags) {
            tag.products.remove(this);
            tag.save();
        }
        super.delete();
    }
    private static List<Product> products;

}





