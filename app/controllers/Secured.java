package controllers;

import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;
import static play.mvc.Results.redirect;

/**
 * Created by Nguyen_Thach on 03/09/2015.
 */
public class Secured extends Security.Authenticator {

    @Override
    public String getUsername(Context ctx) {
        return ctx.session().get("email");
    }

    @Override
    public Result onUnauthorized(Context ctx) {
        return redirect(routes.Application.login());
    }

}
